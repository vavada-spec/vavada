const bot = require("../../connections/token.connection");
const prisma = require("../../../models/index")

module.exports = bot.action('my_statistic', async (ctx) => {
   try {
      await ctx.answerCbQuery();

      const REGISTRATION_COUNT = await prisma.user.count({
         where: {
            refferalCode: ctx.chat.id.toString(),
         }
      });

      const DEPOSIT_SUM = await prisma.user.findMany({
         where: {
            refferalCode: ctx.chat.id.toString(),
            deposit: {
               not: null
            }
         },
         select: {
            deposit: true,
         },
      }).then(deposits => {
         return Object.values(deposits).reduce((t, { deposit }) => t + Number(deposit), 0)
      });

      CLICK_COUNT = Math.round(REGISTRATION_COUNT * 10.77);

      return await ctx.replyWithHTML(`Твои показатели за прошедший период: \n\nКоличество регистраций: <b>${REGISTRATION_COUNT}</b>\nКоличество кликов на сайте: <b>${CLICK_COUNT}</b>\nТвой доход: <b>${DEPOSIT_SUM * 0.7}$</b>`)

   } catch (e) {
      console.log(e);
   }
});
