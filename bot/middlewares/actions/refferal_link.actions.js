const bot = require("../../connections/token.connection");

module.exports = bot.action('get_refferal_link', async (ctx) => {
   try {
      await ctx.answerCbQuery();
      return await ctx.replyWithHTML(`Вот твоя <a href="vavadastd.com/r/${ctx.chat.id}">реферальная ссылка</a>.\n\nКопируй ее и используй в своих кампаниях!`, {
            disable_web_page_preview: true,
        })
   } catch (e) {
      console.log(e);
   }
});
