const game = require("../controllers/game.controller.js");
const router = require("express").Router();

module.exports = (app) => {

    router.get("/:name", game.getImage);

    app.use('/api/v1/images', router);
};
