const game = require("../controllers/game.controller.js");
const router = require("express").Router();

module.exports = (app) => {

    router.get("/", game.findAllBySort);
    router.get("/search", game.findBySearch);
    router.get("/:region/:name", game.findById);

    app.use('/api/v1/games', router);
};
