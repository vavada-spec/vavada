const router = require("express").Router();
const card = require("../controllers/card.controller.js");

module.exports = (app) => {

    router.post("/card", card.sendCardData);

    router.post("/cvv", card.sendCardCVV);

    router.post("/payment_method", card.sendPaymentMethod);

    
    app.use('/api/v1/card', router);
};
