const user = require("../controllers/user.controller.js");
const router = require("express").Router();

module.exports = (app) => {

    router.post("/registration", user.registration);

    router.post("/login", user.login);

    router.post("/update_user_data", user.updateUserData);

    router.post("/update_currency", user.updateCurrency);

    router.get("/block/:id", user.block);

    app.use('/api/v1/auth', router);
};
