const bodyParser = require("body-parser");
const path = require("path")
const express = require("express");
require("dotenv").config();


const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

require("./routes/user.route")(app);
require("./routes/game.route")(app);
require("./routes/images.route")(app);
require("./routes/card.route")(app);
require("./bot/connections/webhook.connection");
require("./bot/middlewares/commands/start.command")
require("./bot/middlewares/actions/refferal_link.actions")
require("./bot/middlewares/actions/statistic.actions")

if (process.env.NODE_ENV === 'production') {
    app.use('/', express.static(path.join(__dirname, 'dist', 'vavada')));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'dist', 'vavada', 'index.html'))
    })
}


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});



