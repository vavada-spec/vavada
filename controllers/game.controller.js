const prisma = require("../models/index");

exports.findAllBySort = async (req, res) => {
    if (req.query) { }
    let { page, type } = req.query;
    const take = 10;
    const skip = page * take;
    let dbReq = {};

    switch (type) {
        case 'slots':
            type = 1;
            dbReq = {
                take,
                skip,
                orderBy: {
                    id: 'asc',
                },
                where: { type }
            }
            break;
        case 'casino':
            type = 2
            dbReq = {
                take,
                skip,
                orderBy: {
                    id: 'asc',
                },
                where: { type }
            }
            break;
        case 'live':
            type = 3;
            dbReq = {
                take,
                skip,
                orderBy: {
                    id: 'asc',
                },
                where: { type }
            }
            break;
        case 'tournament':
            type = 4;
            dbReq = {
                take,
                skip,
                orderBy: {
                    id: 'asc',
                },
                where: { type }
            }
            break;
        default:
            dbReq = {
                take,
                skip,
                orderBy: {
                    id: 'asc',
                }
            }
            break;
    }

    prisma.game.findMany(
        dbReq
    )
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};


exports.findById = async (req, res) => {
    const { region, name } = req.params;
    await prisma.game.findFirst({
        select: {
            link: true
        },
        where: { name }
    })
        .then(result => {

            result = result.link.replace("{REGION}", region)
            res.json(result);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while finding game!"
            });
        })
};

exports.getImage = async (req, res) => {
    const { name } = req.params;
    res.sendFile(`/assets/${name}.jpeg`, { root: "." }, function (err) {
        if (err) {
            if (err.statusCode === 404) {
                res.sendFile(`/assets/${name}.jpg`, { root: "." }, function (err) {
                    if (err) {
                        res.status(500).send({
                            message:
                                err.message || "Some error occurred while finding game!"
                        });
                    }
                })
            } else {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while finding game!"
                });
            }

        }
    })
};

exports.findBySearch = async (req, res) => {
    if (req.query) {
        let { name } = req.query;
        if (name) {
            const take = 10;

            await prisma.game.findMany({
                take,
                orderBy: {
                    id: 'asc',
                },

                where: {
                    name: {
                        contains: name,
                        mode: 'insensitive',
                    },
                    link: {
                        not: ''
                    }
                }

            })
                .then(data => {
                    res.send(data);
                })
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while retrieving tutorials."
                    });
                });
        } else {
            res.send([])
        }
    }
};


