const prisma = require("../models/index")


exports.registration = async (req, res) => {
    if (!req.body) {
        return await res.status(400).send(
            { message: "ALL_FIELDS_REQUIERED" });
    } else {
        const { login, password, currency, refferalCode, language } = req.body;
        if (login == '' || password == '') { return await res.status(400).send({ message: "ALL_FIELDS_REQUIERED" }) }
        else
            if (isEmailValid(login)) {
                const name = 'Player66418';
                const email = login
                const findUser = await prisma.user.findFirst({ where: { email } });

                if (findUser) {
                    await res.status(300).send(
                        { message: "ALREADY_EXIST" }
                    )
                } else {
                    return await prisma.user.create({
                        data: { password, email, name, refferalCode, currency, language, deposit: '0' }
                    })
                        .then(data => {
                            res.send(data);
                        })
                        .catch(err => {
                            res.status(500).send({
                                message: "FAILED_CREATING_ACCOUNT"
                            });
                        });
                }
            } else if (isPhoneValid(login)) {
                const name = 'Player66418';
                const phone = login
                const findUser = await prisma.user.findFirst({ where: { phone } });

                if (findUser) {
                    await res.status(300).send(
                        { message: "ALREADY_EXIST" }
                    )
                } else {
                    return await prisma.user.create({ data: { password, phone, name, refferalCode, currency, language, deposit: '0' } })
                        .then(data => {
                            res.send(data);
                        })
                        .catch(err => {
                            res.status(500).send({
                                message: "FAILED_CREATING_ACCOUNT"
                            });
                        });
                }
            } else {
                return await res.status(400).send({ message: "FAILED_LOGIN" })
            }
    }
};

exports.login = async (req, res) => {
    if (!req.body) {
        return await res.status(400).send(
            { message: "ALL_FIELDS_REQUIERED" });
    } else {
        const { login, password } = req.body;
        if (login == '' || password == '') { return await res.status(400).send({ message: "ALL_FIELDS_REQUIERED" }) } else
            if (isEmailValid(login)) {
                const email = login;
                await prisma.user.findFirst({ where: { email } })
                    .then(async data => {
                        if (data) {
                            await res.send(data);
                        } else {
                            res.status(500).send({
                                message: "DOES_NOT_EXIST"
                            });
                        }
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: "DOES_NOT_EXIST"
                        });
                    });
            } else if (isPhoneValid(login)) {
                const phone = login;
                await prisma.user.findFirst({ where: { phone } })
                    .then(data => {
                        if (data) {
                            res.send(data);
                        } else {
                            res.status(500).send({
                                message: "DOES_NOT_EXIST"
                            });
                        }
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: "DOES_NOT_EXIST"
                        });
                    });
            } else {
                return await res.status(400).send({ message: "FAILED_LOGIN" })
            }
    }
};

exports.updateUserData = async (req, res) => {
    if (!req.body) {
        return await res.status(400).send(
            { message: "ALL_FIELDS_REQUIERED" });
    } else {
        // const userUpdate = req.body;
        if (!req.body.email && !req.body.phone) {
            return res.status(400).send({ message: "LOGIN_FIELDS_REQUIRED" })
        } else {
            const userUpdate = req.body;
            userUpdate.id = parseInt(userUpdate.id)

            await prisma.user.update({
                where: {
                    id: userUpdate.id,
                },
                data: userUpdate,
            })
                .then(data => {
                    if (data) {
                        res.send(data)
                    } else {
                        res.status(500).send({
                            message: "DOES_NOT_EXIST"
                        });
                    }
                })
                .catch(err => {
                    res.status(500).send({
                        message: "DOES_NOT_EXIST"
                    });
                });

        }
    }
}

exports.updateCurrency = async (req, res) => {
    if (!req.body) {
        return await res.status(400).send(
            { message: "ALL_FIELDS_REQUIERED" });
    } else {
        let { id, currency } = req.body;
        id = parseInt(id)

        await prisma.user.update({
            where: {
                id,
            },
            data: { currency },
        })
            .then(data => {
                if (data) {
                    res.send(data)
                } else {
                    res.status(500).send({
                        message: "DOES_NOT_EXIST"
                    });
                }
            })
            .catch(err => {
                res.status(500).send({
                    message: "DOES_NOT_EXIST"
                });
            });
    }
}

exports.block = async (req, res) => {
    if (!req.params) {
        return await res.status(400).send(
            { message: "ALL_FIELDS_REQUIERED" });
    } else {
        const id = +req.params.id;
        await prisma.user.update({
            where: {
                id,
            },
            data: { is_blocked: true, },
        })
            .then(data => {
                res.send(data)
            })
            .catch(err => {
                res.status(500).send({
                    message: "DOES_NOT_EXIST"
                });
            });

    }

}



function isEmailValid(email) {
    var emailRegex = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

    if (!email)
        return false;

    if (email.length > 254)
        return false;

    var valid = emailRegex.test(email);
    if (!valid)
        return false;

    // Further checking of some things regex can't handle
    var parts = email.split("@");
    if (parts[0].length > 64)
        return false;

    var domainParts = parts[1].split(".");
    if (domainParts.some(function (part) { return part.length > 63; }))
        return false;

    return true;
}

function isPhoneValid(phone) {
    const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
    return regex.test(phone)
}
